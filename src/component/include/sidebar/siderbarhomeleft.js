import React, { Component } from "react";
import Nav from 'react-bootstrap/Nav';
import Tab from 'react-bootstrap/Tab';

class Sidebarhomeleft extends Component{
    render(){
        return(
            <>
            
           <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                
                    
                   
                    <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                        <Nav.Link eventKey="first">Home</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                        <Nav.Link eventKey="2">Notifications</Nav.Link>
                        <Nav.Link eventKey="3">Team Finder</Nav.Link>
                        <Nav.Link eventKey="4">Tournament</Nav.Link>
                        <p>Games</p>
                        <Nav.Link eventKey="5">League of Legends</Nav.Link>
                        <Nav.Link eventKey="6">CS:GO</Nav.Link>
                        <Nav.Link eventKey="7">Dota 2</Nav.Link>
                        <Nav.Link eventKey="8">Rocket League</Nav.Link>
                        <Nav.Link eventKey="9">Teamfight Tactics</Nav.Link>
                        
                        </Nav.Item>
                    </Nav>
                
                   
              
                </Tab.Container>
            </>
        );
    }
}
export default Sidebarhomeleft;