import React, { Component } from "react";
import Image from 'react-bootstrap/Image'
import Cimage from '../../assets/images/cardimg.png';
class Cards extends Component{
    render(){
        return(
            <>
            <Image className="img-fluid" src={Cimage} rounded />
            <p className="p mt-3">
                PUBG MOBILE</p>
            </>
        );
    }
}
export default Cards;