import React, { Component } from "react";
import Slider from "../component/include/slider";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Cards from "./include/cards";
import Cardss from "./include/card2";
import Cardsss from "./include/cards3";
import Image from 'react-bootstrap/Image';
import imgbanner from "../assets/images/slider/s1.jpg";
import profile from "../assets/images/slider/profile.jpg";
import Card4 from "./include/card4";
import Card5 from "./include/card5";
import Card6 from "./include/card6";
import Sidebarhome from "./include/sidebar/siderbarhome";
import Sidebarhomeleft from "./include/sidebar/siderbarhomeleft";
import Sidebarhomeright from "./include/sidebar/siderbarhomeright";

class Home extends Component{
    render(){
        return(
            <>
            <Container fluid>
            <Row>
            <Col xl={1} className="mt-2 mb-5">
                <Sidebarhome></Sidebarhome>
            </Col>
            <Col xl={2} className="mb-3">
                <Row className="text-center mt-5 mb-5">
                    <Col xl={12}>
                    <Image className="img-fluid121" roundedCircle  src={profile}  />
                    </Col>
                </Row>
                <Sidebarhomeleft></Sidebarhomeleft>
            </Col>
            
                <Col xl={7}>
                <Slider></Slider>
                <Row className="mt-5 p p-2">
                        <Col xl={6} >
                            <h6>
                                Browse Games
                            </h6>
                        </Col>
                        <Col xl={6} className="text-right yellow">
                            View All
                        </Col>
                </Row>
                <Row className="mt-2 mb-2">
                    
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                </Row>
                <Row className="mt-3 mb-2 p p-2">
                        <Col xl={6} >
                            <h6>
                            Recommended Tournaments
                            </h6>
                        </Col>
                        
                </Row>
                <Row className="mt-2 mb-2">
                    
                    <Col xl={3} md={2} xs={12}>
                        <Cardss></Cardss>
                        
                    </Col>
                    <Col xl={3} md={2} xs={12}>
                    <Cardss></Cardss>
                    </Col>
                    <Col xl={3} md={2} xs={12}>
                    <Cardss></Cardss>
                    </Col>
                    <Col xl={3} md={2} xs={12}>
                    <Cardss></Cardss>
                    </Col>
                </Row>
                <Row className="mt-5 p p-2">
                        <Col xl={6} >
                            <h6>
                            Trending Spaces
                            </h6>
                        </Col>
                        <Col xl={6} className="text-right yellow">
                            View All
                        </Col>
                </Row>
                <Row className="mt-4 mb-2 text-center">
                    
                    <Col xl={2} md={2} xs={12}>
                    <Cardsss></Cardsss>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        <Cards></Cards>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                        
                        <Cardsss></Cardsss>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                    <Cardsss></Cardsss>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                    <Cardsss></Cardsss>
                    </Col>
                    <Col xl={2} md={2} xs={12}>
                    <Cardsss></Cardsss>
                    </Col>
                </Row>
                <Row>
                    <Col xl={12}>
                    <Image className="img-fluid" src={imgbanner}  />
                    </Col>
                </Row>
                <Row className="mt-5 p p-2">
                        <Col xl={6} >
                            <h6>
                            Trending Tournaments
                            </h6>
                        </Col>
                        <Col xl={6} className="text-right yellow">
                            View All
                        </Col>
                </Row>
                <Row className="mt-4 mb-2 ">
                    
                    <Col xl={3} md={3} xs={12}>
                    <Card4></Card4>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                    <Card4></Card4>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                        
                    <Card4></Card4>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                    <Card4></Card4>
                    </Col>
                    
                </Row>
                <Row className="mt-5 p p-2">
                        <Col xl={6} >
                            <h6>
                            Trending Tournaments
                            </h6>
                        </Col>
                       
                </Row>
                <Row className="mt-4 mb-2 ">
                    
                    <Col xl={3} md={3} xs={12}>
                    
                    <Card5></Card5>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                    <Card5></Card5>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                        
                    <Card5></Card5>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                    <Card5></Card5>
                    </Col>
                    
                </Row>
                <Row className="mt-5 p p-2">
                        <Col xl={6} >
                            <h6>
                            Tournaments With Large Prizes
                            </h6>
                        </Col>
                       
                </Row>
                <Row className="mt-4 mb-2 ">
                    
                    <Col xl={3} md={3} xs={12}>
                    
                    <Card6></Card6>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                    <Card6></Card6>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                        
                    <Card6></Card6>
                    </Col>
                    <Col xl={3} md={3} xs={12}>
                    <Card6></Card6>
                    </Col>
                    
                </Row>
                
                </Col>
                <Col xl={2}>
                   <Sidebarhomeright></Sidebarhomeright>
                </Col>
                </Row>
                </Container>
           </>
        );
    }
}
export default Home;