import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/main.css';
import Home from './component/home';

function App() {
  return (
    <Home></Home>
  );
}

export default App;
